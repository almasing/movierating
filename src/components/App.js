import React from 'react'
import { Switch, Route } from 'react-router-dom';
import Main from './Main';
import "bootstrap/dist/css/bootstrap.css";
import About from './About';
import Header from './Header';
import Footer from "./Footer";

const App = () => (
  <div>
    <Header/>
    <main>
      <Switch>
        <Route exact path="/" component={Main} />
        <Route exact path="/about" component={About} />
      </Switch>
    </main>
    <Footer/>
  </div>
);

export default App;
