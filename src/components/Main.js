import React, { Component } from "react";
import MovieCard from "./MovieCard";
import SearchBarAndFilter from "./SearchBarAndFilter";

import axios from "axios";

class Main extends Component {
  constructor() {
    super();

    this.state = {
      activeTab: 1,
      queryString: "",
      filteredItems: [],
      from: 0,
      to: 10
    };
  }

  componentDidMount() {
    // User information and/or tokens would usually be saved to a session, or local storage upon login
    // Since I did not implement login, I'm hardcoding just this user Guid, so I could pass it to the back end
    // The user behind it does not have any ratings added to the DB
    sessionStorage.setItem(
      "userGuid",
      "66983dde-f784-4a51-a0a0-bcf9cccebe9a"
    );

    // This is just to add the userGuid from the sessionStorage to the request header

    axios.interceptors.request.use(
      config => {
        const token = sessionStorage.getItem("userGuid");
        if (true) config.headers.UserGuid = token;
        return config;
      },
      function(error) {
        return Promise.reject(error);
      }
    );

    // API call
    this.fetchData();
  }

  getActiveTab = () => {
    return this.state.activeTab;
  };

  setActiveTab = tab => {
    this.setState({ activeTab: tab }, () => {
      this.fetchData();
    });
  };

  setQueryString = queryString => {
    this.setState({ queryString }, () => {
      this.fetchData();
    });
  };

  setUserRating = (nextValue, prevValue, name, item) => {
    // First set the state locally by updating a particular movie's userRating
    this.setState(state => {
      const filteredItems = state.filteredItems;

      filteredItems.forEach(stateItem => {
        if (stateItem.$id === item.$id) {
          stateItem.userRating = nextValue;
        }
      });

      return {
        filteredItems
      };
    });

    // Then make an API call which will update/insert the record in the DB
    var config = {
      crossdomain: true
    };

    axios
      .post(
        `http://localhost:54455/api/Ratings`,
        {
          // The object we're passing to the API, containing the new rating
          userGuid: sessionStorage.getItem("userGuid"),
          movieOrTvShowGuid: item.guid,
          rate: nextValue
        },
        { config }
      )
      .then(response => {
        // Re-fetch the data, because average rating has been affected by changing the user rating
        this.fetchData();
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleLoadMore = () => {
    this.setState({ to: this.state.to + 10 }, () => {
      this.fetchData();
    });
  };

  fetchData = () => {
    var config = {
      crossdomain: true
    };

    // The API call
    axios
      .get(
        `http://localhost:54455/api/MoviesAndShows/${this.state.from}/${this.state.to}/${this.state.activeTab}/${this.state.queryString}`,
        { config }
      )
      .then(response => {
        this.setState({
          filteredItems: response.data
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    const { filteredItems, queryString, activeTab } = this.state;
    return (
      <div>
        <div>
          <SearchBarAndFilter
            activeTab={activeTab}
            setActiveTab={this.setActiveTab}
            queryString={queryString}
            setQueryString={this.setQueryString}
          />
        </div>

          {filteredItems.map(item => {
            return (
              <MovieCard
                item={item}
                key={item.$id}
                setUserRating={this.setUserRating}
              />
            );
          })}

        <div className="viewMoreResults">
          <button onClick={this.handleLoadMore} className="btn btn-primary">
            View More results
          </button>
        </div>
      </div>
    );
  }
}

export default Main;
