import React, { Component } from "react";

class About extends Component {

    render() { 
        return (
          <div class="alert alert-primary" role="alert">
            No content here. The purpose of this page is solely to demonstrate router usage.
          </div>
        );
    }
};
 
export default About;