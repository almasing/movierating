import React from "react";

function Footer () {
  return (
    <footer>
        <div className="text-center small copyright footer">© Alma Aganovic 2019</div>
    </footer>
  );
}

export default Footer;
