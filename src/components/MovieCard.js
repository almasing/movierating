import React, { Component } from "react";
import StarRatingComponent from "react-star-rating-component";
import "bootstrap/dist/css/bootstrap.css";

class MovieCard extends Component {
  constructor() {
    super();
    this.state = {
      userRating: 0
    };
  }

  render() {
    return (
      <div className="movieCardWrapper">
        <div className="card movieCard ">
          <div>
            <img
              src={this.props.item.poster}
              className="card-img-left"
              alt="..."
            />
          </div>
          <div className="card-body rating">
            <div>
              <h5 className="card-title">{this.props.item.title}</h5>{" "}
              <div style={{ float: "right" }}>
                {this.props.item.releaseDate}
              </div>
              <div className="ratingContainer">
                <StarRatingComponent
                  name={this.props.item.title + "Rating"}
                  starCount={5}
                  value={this.props.item.rating}
                />
                <div className="numericalRating">
                  {this.props.item.rating.toFixed(2)} / 5
                </div>
              </div>
            </div>
            <p className="card-text">{this.props.item.overview}</p>
            <div className="castWrapper">
              <div className="castKeyword">Cast:</div>{" "}
              {this.props.item.cast.join(", ")}
            </div>
            <div className="userRating card-body">
              <StarRatingComponent
                name="userRating"
                starCount={5}
                value={this.props.item.userRating}
                onStarClick={(nextValue, prevValue, name) => {
                  this.props.setUserRating(
                    nextValue,
                    prevValue,
                    name,
                    this.props.item
                  );
                }}
              />
              <div>Leave your rating!</div>
            </div>
          </div>
          <br />
        </div>
      </div>
    );
  }
}
export default MovieCard;
