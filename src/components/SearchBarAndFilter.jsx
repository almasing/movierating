import React, { Component } from "react";
import { DebounceInput } from "react-debounce-input";

class SearchBarAndFilter extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="searchBarContainer">
        <DebounceInput
          minLength={2}
          debounceTimeout={300}
          onChange={event => this.props.setQueryString(event.target.value)}
        />
        <div className="searchTabControl shadow-sm">
          <div
            id="moviesTab"
            className={this.props.activeTab === 1 ? "active" : ""}
            ref="movies"
            onClick={() => this.props.setActiveTab(1)}
          >
            Movies
          </div>
          <div
            id="tvShowsTab"
            className={this.props.activeTab === 2 ? "active" : ""}
            ref="tvShows"
            onClick={() => this.props.setActiveTab(2)}
          >
            Tv Shows
          </div>
        </div>
      </div>
    );
  }
}

export default SearchBarAndFilter;
